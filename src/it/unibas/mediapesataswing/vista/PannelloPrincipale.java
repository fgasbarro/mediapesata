package it.unibas.mediapesataswing.vista;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.controllo.Controllo;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

public class PannelloPrincipale extends JPanel {
    
    private Controllo controllo;
    private Vista vista;
    private Modello modello;
    private Map mappaSottoViste = new HashMap();
    
    public Object getSottoVista(String nome) {
        return this.mappaSottoViste.get(nome);
    }
    
    public PannelloPrincipale(Vista vista) {
        this.vista = vista;
        this.controllo = vista.getControllo();
        this.modello = vista.getModello();
        this.inizializza();
    }
    
    private boolean visualizzaLista = true;
    
    private JList jListaEsami = new JList();
    private JTable jTabellaEsami = new JTable();
    private JScrollPane scrollPane;
    private JLabel labelNome = new JLabel();
    private JLabel labelCognome = new JLabel();
    private JLabel labelMatricola = new JLabel();
    private JLabel labelMediaPesata = new JLabel();
    private JLabel labelMedia110 = new JLabel();
    private JTextField campoCrediti = new JTextField();
    private JTextField campoInsegnamento = new JTextField();
    private JTextField campoVoto = new JTextField();
    private JCheckBox checkBoxLode = new JCheckBox();
    
    public String getCampoInsegnamento() {
        return this.campoInsegnamento.getText();
    }
    
    public String getCampoCrediti() {
        return this.campoCrediti.getText();
    }
    
    public String getCampoVoto() {
        return this.campoVoto.getText();
    }
    
    public boolean getCheckLode() {
        return this.checkBoxLode.isSelected();
    }
    
    public int getSelectedIndex() {
        if (visualizzaLista) {
            return this.jListaEsami.getSelectedIndex();
        }
        return this.jTabellaEsami.getSelectedRow();
    }
    
    /* ********************************
     *     Inizializzazione
     * ****************************** */
    
    public void inizializza() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.jListaEsami.setVisibleRowCount(20);
        creaPannelloDatiStudente();
        creaPannelloInserisciEsame();
        creaPannelloListaEsami();
        creaPannelloMedia();
    }
    
    private void creaPannelloDatiStudente() {
        JPanel pannelloDatiStudente = new JPanel();
        pannelloDatiStudente.setBorder(creaBordoTitolo("Dati dello Studente"));
        JLabel label1 = new JLabel("    Cognome: ");
        JLabel label2 = new JLabel("    -    Nome: ");
        JLabel label3 = new JLabel("    -    Matricola: ");
        JButton bottoneModifica = new JButton(this.controllo.getAzione(Costanti.AZIONE_MODIFICA_STUDENTE));
        pannelloDatiStudente.add(label1);
        pannelloDatiStudente.add(this.labelNome);
        pannelloDatiStudente.add(label2);
        pannelloDatiStudente.add(this.labelCognome);
        pannelloDatiStudente.add(label3);
        pannelloDatiStudente.add(this.labelMatricola);
        pannelloDatiStudente.add(Box.createRigidArea(new Dimension(20, 20)));
        pannelloDatiStudente.add(bottoneModifica);
        this.add(pannelloDatiStudente);
    }
    
    private void creaPannelloInserisciEsame() {
        JPanel pannelloEsame = new JPanel();
        JLabel labelInsegnamento = new JLabel("Insegnamento");
        campoInsegnamento.setColumns(20);
        pannelloEsame.add(labelInsegnamento);
        pannelloEsame.add(campoInsegnamento);
        JLabel labelCrediti = new JLabel("Crediti");
        campoCrediti.setColumns(3);
        pannelloEsame.add(labelCrediti);
        pannelloEsame.add(campoCrediti);
        JLabel labelVoto = new JLabel("Voto");
        campoVoto.setColumns(2);
        pannelloEsame.add(labelVoto);
        pannelloEsame.add(campoVoto);
        JLabel labelLode = new JLabel("Lode");
        pannelloEsame.add(labelLode);
        pannelloEsame.add(checkBoxLode);
        pannelloEsame.setBorder(creaBordoTitolo("Inserisci un nuovo esame"));
        Action azioneInserisciEsame = this.controllo.getAzione(Costanti.AZIONE_INSERISCI_ESAME);
        JButton bottoneInserisciEsame = new JButton(azioneInserisciEsame);
        campoInsegnamento.setAction(azioneInserisciEsame);
        campoCrediti.setAction(azioneInserisciEsame);
        campoVoto.setAction(azioneInserisciEsame);
        pannelloEsame.add(bottoneInserisciEsame);
        this.disabilitaControlliEsame();
        this.add(pannelloEsame);
    }
    
    private void creaPannelloListaEsami() {
        JPanel pannelloListaEsami = new JPanel();
        pannelloListaEsami.setLayout(new BoxLayout(pannelloListaEsami, BoxLayout.Y_AXIS));
        this.scrollPane = new JScrollPane();
        this.scrollPane.setViewportView(this.jListaEsami);
        this.jListaEsami.setVisibleRowCount(20);
        pannelloListaEsami.add(scrollPane);
        JPanel pannelloBottoni = new JPanel();
        JButton bottoneModificaEsame = new JButton(this.controllo.getAzione(Costanti.AZIONE_MODIFICA_ESAME));
        JButton bottoneEliminaEsame = new JButton(this.controllo.getAzione(Costanti.AZIONE_ELIMINA_ESAME));
        pannelloBottoni.add(bottoneModificaEsame);
        pannelloBottoni.add(bottoneEliminaEsame);
        pannelloListaEsami.add(pannelloBottoni);
        pannelloListaEsami.setBorder(creaBordoTitolo("Elenco degli Esami"));
        this.add(pannelloListaEsami);
    }
    
    private void creaPannelloMedia() {
        JPanel pannelloMedia = new JPanel();
        JButton bottoneMedia = new JButton(this.controllo.getAzione(Costanti.AZIONE_CALCOLA_MEDIA));
        JLabel label1 = new JLabel("    Media pesata: ");
        JLabel label2 = new JLabel("    -    Voto di partenza (in 110mi): ");
        pannelloMedia.add(label1);
        pannelloMedia.add(this.labelMediaPesata);
        pannelloMedia.add(label2);
        pannelloMedia.add(this.labelMedia110);
        pannelloMedia.add(Box.createRigidArea(new Dimension(20, 20)));
        pannelloMedia.add(bottoneMedia);
        this.add(pannelloMedia);
    }
    
    private Border creaBordoTitolo(String titolo) {
        Border bordoVuoto = BorderFactory.createEmptyBorder(5, 10, 5, 10);
        Border bordoContorno = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
        Border bordoComposto = BorderFactory.createCompoundBorder(bordoContorno, bordoVuoto);
        Border bordoTitolo = BorderFactory.createTitledBorder(bordoComposto, titolo);
        return bordoTitolo;
    }
    
    /* ********************************
     *     Schermo Studente
     * ****************************** */
    
    public void schermoNuovoStudente() {
        this.labelNome.setText("");
        this.labelCognome.setText("");
        this.labelMatricola.setText("");
        this.aggiornaEsami();
        this.labelMediaPesata.setText("");
        this.labelMedia110.setText("");
        this.abilitaControlliEsame();
    }
    
    public void schermoStudente() {
        Modello modello = this.controllo.getModello();
        Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
        this.labelNome.setText(studente.getNome());
        this.labelCognome.setText(studente.getCognome());
        this.labelMatricola.setText(studente.getMatricola() + "");
        this.aggiornaEsami();
        this.abilitaControlliEsame();
    }

    public void ripulisciEsame() {
        this.campoInsegnamento.setText("");
        this.campoCrediti.setText("");
        this.campoVoto.setText("");
        this.checkBoxLode.setSelected(false);
    }
    
    public void schermoMedia() {
        Modello modello = this.controllo.getModello();
        Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
        java.text.DecimalFormat formattatore = new java.text.DecimalFormat("###.##");
        this.labelMediaPesata.setText(formattatore.format(studente.getMediaPesata()));
        this.labelMedia110.setText(formattatore.format(studente.getMediaPartenza()));
    }
    
    public void aggiornaEsami() {
        if (this.visualizzaLista) {
            aggiornaListaEsami();
        } else {
            aggiornaTabellaEsami();
        }
        this.vista.pack();
    }
    
    public void aggiornaListaEsami() {
        Modello modello = this.controllo.getModello();
        Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
        ModelloLista modelloListaEsami = new ModelloLista(studente);
        this.jListaEsami.setModel(modelloListaEsami);
        this.scrollPane.setViewportView(this.jListaEsami);
    }
    
    public void aggiornaTabellaEsami() {
        Modello modello = this.controllo.getModello();
        Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
        ModelloTabella modelloTabella = new ModelloTabella(studente);
        this.jTabellaEsami.setModel(modelloTabella);
        this.scrollPane.setViewportView(this.jTabellaEsami);
    }
    
    public void passaALista() {
        this.scrollPane.setViewportView(this.jListaEsami);
        this.visualizzaLista = true;
        aggiornaListaEsami();
    }
    
    public void passaATabella() {
        this.scrollPane.setViewportView(this.jTabellaEsami);
        this.visualizzaLista = false;
        aggiornaTabellaEsami();
    }

    public void disabilitaControlliEsame() {
        this.campoCrediti.setEnabled(false);
        this.campoInsegnamento.setEnabled(false);
        this.campoVoto.setEnabled(false);
        this.checkBoxLode.setEnabled(false);
    }

    public void abilitaControlliEsame() {
        this.campoCrediti.setEnabled(true);
        this.campoInsegnamento.setEnabled(true);
        this.campoVoto.setEnabled(true);
        this.checkBoxLode.setEnabled(true);
    }
}