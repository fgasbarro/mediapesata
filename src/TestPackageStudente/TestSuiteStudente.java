package TestPackageStudente;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ noValidEliminaEsameTest2.class, noValidEsameTest2.class, noValidMediaTest2.class,
		validEliminaEsameTest2.class, validEsameTest2.class, validMediaTest2.class })
public class TestSuiteStudente {

}
