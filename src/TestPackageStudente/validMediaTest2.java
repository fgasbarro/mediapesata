package TestPackageStudente;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import it.unibas.mediapesataswing.modello.Esame;
import it.unibas.mediapesataswing.modello.Studente;

public class validMediaTest2 {

	@Test
	public void test() {
double mediaprevista=29.2;
		
		String nome = "Francesco";
		String cognome = "Gasbarro";
		int matricola =656604;
		Studente studente= new Studente (nome, cognome, matricola);
		
		String materia = "Matematica Discreta";
		int voto = 30;
		boolean lode = true;
		int crediti = 9;
		Esame esame1 = new Esame(materia, voto, lode, crediti);
		
		String materia2 = "Calcolo Numerico";
		int voto2 = 28;
		boolean lode2 = false;
		int crediti2 = 6;
		Esame esame2 = new Esame(materia2, voto2, lode2, crediti2);
	
		studente.addEsame(esame1);
		studente.addEsame(esame2); 

		assertEquals(mediaprevista, studente.getMediaPesata());
	}

}
