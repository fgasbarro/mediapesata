package TestPackageStudente;

import static org.junit.Assert.*;

import org.junit.Test;

import it.unibas.mediapesataswing.modello.Esame;
import it.unibas.mediapesataswing.modello.Studente;

public class noValidEsameTest2 {

	@Test
	public void test() {
		String nome = "Francesco";
		String cognome = "Gasbarro";
		int matricola =656604;
		Studente studente= new Studente (nome, cognome, matricola);
		
		String materia = "Integrazione e Test di Sitemi Software";
		int voto = 30;
		boolean lode = true;
		int crediti = 9;
		Esame esame1 = new Esame(materia, voto, lode, crediti);
		
		String materia2 = "Ingegneria del Software";
		int voto2 = 30;
		boolean lode2 = true;
		int crediti2 = 9;
		Esame esame2 = new Esame(materia2, voto2, lode2, crediti2);
	
		studente.addEsame(esame1);
		studente.addEsame(esame2);

		studente.getEsame(2);
	}

}
