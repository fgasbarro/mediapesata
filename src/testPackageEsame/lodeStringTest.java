package testPackageEsame;

import static org.junit.Assert.*;

import org.junit.Test;

import it.unibas.mediapesataswing.modello.Esame;

public class lodeStringTest {

	@Test
	public void test() {
		String materia = "Storia";
		int voto = 30;
		boolean lode = true;
		int crediti = 9;
		Esame esame = new Esame(materia, voto, lode, crediti);
		String output = esame.toString();
		assertEquals("Esame di " + materia + " (" + crediti + " CFU) - voto: " + voto + " e lode", output);
	}

}
