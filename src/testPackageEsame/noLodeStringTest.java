package testPackageEsame;

import static org.junit.Assert.*;

import org.junit.Test;

import it.unibas.mediapesataswing.modello.Esame;

public class noLodeStringTest {

	@Test
	public void test() {
		String materia = "Storia";
		int voto = 30;
		boolean lode = false;
		int crediti = 9;
		Esame esame = new Esame(materia, voto, lode, crediti);
		String output = esame.toString();
		assertEquals("Esame di " + materia + " (" + crediti + " CFU) - voto: " + voto, output);
	}

}
