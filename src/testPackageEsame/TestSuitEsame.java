package testPackageEsame;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ lodeStringTest.class, noLodeStringTest.class, setCreditiNoValidTest.class, setCreditiValidTest.class,
		setLodeNoValidTest.class, setLodeValidTest.class, setVotoValidTest.class, seVotoNoValidTest.class })
public class TestSuitEsame {

}
